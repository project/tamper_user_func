<?php

namespace Drupal\tamper_user_func\Plugin\Tamper;

use Drupal\Core\Form\FormStateInterface;
use Drupal\tamper\Exception\SkipTamperItemException;
use Drupal\tamper\Exception\TamperException;
use Drupal\tamper\TamperableItemInterface;
use Drupal\tamper\TamperBase;

/**
 * Plugin implementation for calculating a value.
 *
 * @Tamper(
 *   id = "user_func",
 *   label = @Translation("User Func"),
 *   description = @Translation("Call a custom PHP function using tokens as args."),
 *   category = "Custom"
 * )
 */
class UserFunc extends TamperBase {

  const BEHAVIOR_SET_DEFAULT_VALUE = 'set_default_value';
  const BEHAVIOR_SKIP = 'skip';
  const BEHAVIOR_RAISE_EXCEPTION = 'raise_exception';

  const SETTING_FUNCTION = 'function';
  const SETTING_ARGS = 'args';
  const SETTING_NO_FUNCTION_BEHAVIOR = 'no_function_behavior';
  const SETTING_EXCEPTION_BEHAVIOR = 'setting_exception_behavior';
  const SETTING_DEFAULT_VALUE = 'default_value';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    $config[self::SETTING_FUNCTION] = '';
    $config[self::SETTING_ARGS] = '';
    $config[self::SETTING_NO_FUNCTION_BEHAVIOR] = self::BEHAVIOR_SET_DEFAULT_VALUE;
    $config[self::SETTING_EXCEPTION_BEHAVIOR] = self::BEHAVIOR_SKIP;
    $config[self::SETTING_DEFAULT_VALUE] = '';
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form[self::SETTING_FUNCTION] = [
      '#type' => 'textfield',
      '#title' => $this->t('PHP function name'),
      '#default_value' => $this->getSetting(self::SETTING_FUNCTION),
      '#description' => $this->t('The function to use.'),
    ];

    $form[self::SETTING_ARGS] = [
      '#type' => 'textarea',
      '#title' => $this->t('Arguments'),
      '#default_value' => $this->getSetting(self::SETTING_ARGS),
      '#description' => $this->t('Use "," (comma) to separate, "\\," for comma as part of value, "\\~" for no symbol (to break pattern keys like this: "[\\~value]") and "\\ " for leading and trailing spaces to avoid trimming'),
    ];

    $replace = [];
    foreach ($this->sourceDefinition->getList() as $source) {
      $replace[] = '[' . $source . ']';
    }

    $form[self::SETTING_NO_FUNCTION_BEHAVIOR] = [
      '#type' => 'radios',
      '#title' => $this->t('No function Behavior'),
      '#options' => $this->getFallbackOptions(),
      '#default_value' => $this->getSetting(self::SETTING_NO_FUNCTION_BEHAVIOR),
      '#description' => $this->t('Behavior if the function does not exist.'),
    ];

    $form[self::SETTING_EXCEPTION_BEHAVIOR] = [
      '#type' => 'radios',
      '#title' => $this->t('Exception Behavior'),
      '#options' => $this->getFallbackOptions(),
      '#default_value' => $this->getSetting(self::SETTING_EXCEPTION_BEHAVIOR),
      '#description' => $this->t('Behavior if the function throws exception.'),
    ];

    $form[self::SETTING_DEFAULT_VALUE] = [
      '#type' => 'textarea',
      '#title' => $this->t('Default value'),
      '#default_value' => $this->getSetting(self::SETTING_DEFAULT_VALUE),
    ];

    $form['help'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Available replacement patterns'),
      '#description' => $this->t('Patterns can be used for both Arguments and Default Value.'),
      'list' => [
        '#theme' => 'item_list',
        '#items' => $replace,
      ],
    ];

    return $form;
  }

  /**
   * Get the fallback options.
   *
   * @return array
   *   The list of options with labels
   */
  protected function getFallbackOptions() {
    return [
      self::BEHAVIOR_SET_DEFAULT_VALUE => $this->t('Set Default Value'),
      self::BEHAVIOR_SKIP => $this->t('Skip (make an item to not be processed)'),
      self::BEHAVIOR_RAISE_EXCEPTION => $this->t('Raise an exception (break process)'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function tamper($data, TamperableItemInterface $item = NULL) {
    if (is_null($item)) {
      // Nothing to rewrite.
      return $data;
    }

    $trans = [];
    foreach ($item->getSource() as $key => $value) {
      $trans['[' . $key . ']'] = is_array($value) ? reset($value) : $value;
    }

    $functionName = $this->getSetting(self::SETTING_FUNCTION);

    if (!function_exists($functionName)) {
      $errorMessage = sprintf('Function "%s" does not exist.', $functionName);
      $this->processError(self::SETTING_NO_FUNCTION_BEHAVIOR, $errorMessage, $trans);
    }

    $rawArgs = strtr($this->getSetting(self::SETTING_ARGS), $trans);
    $rawArgs = str_replace("\\~", "", $rawArgs);

    $preList = [];
    $splitText = explode("\\,", $rawArgs);
    foreach ($splitText as $splitTextItem) {
      $preItems = explode(",", $splitTextItem);
      $preList[] = $preItems;
    }

    $args = [];
    $lastItem = FALSE;
    foreach ($preList as $preItem) {
      foreach ($preItem as $index => $item) {
        if ($index === 0 && $lastItem !== FALSE) {
          $item = $lastItem . ',' . $item;
        }
        if ($index < count($preItem) - 1) {
          $args[] = $item;
        }
        $lastItem = $item;
      }
    }
    $args[] = $lastItem;

    foreach ($args as &$arg) {
      $arg = trim($arg);
      $arg = str_replace("\\ ", " ", $arg);
    }
    unset($arg);

    try {
      return call_user_func_array($functionName, $args);
    }
    catch (\Exception $e) {
      $errorMessage = sprintf('Function "%s" threw exception: %s.', $functionName, $e->getMessage());
      return $this->processError(self::SETTING_EXCEPTION_BEHAVIOR, $errorMessage, $trans);
    }
  }

  /**
   * Handle error of processing according to behavior settings.
   *
   * @param string $behaviorSettingKey
   *   The key of behavior settings to use.
   * @param string $errorMessage
   *   The error message.
   * @param array $trans
   *   A arguments for message.
   *
   * @return string
   *   Fallback result if it cab be used
   *
   * @throws \Drupal\tamper\Exception\SkipTamperItemException
   * @throws \Drupal\tamper\Exception\TamperException
   */
  protected function processError(string $behaviorSettingKey, string $errorMessage, array $trans) {
    $value = '';
    switch ($this->getSetting($behaviorSettingKey)) {
      case self::BEHAVIOR_SET_DEFAULT_VALUE:
      default:
        $this->messenger()->addWarning($errorMessage);
        $value = strtr($this->getSetting(self::SETTING_DEFAULT_VALUE), $trans);
        break;

      case self::BEHAVIOR_SKIP:
        $this->messenger()->addError($errorMessage);
        $this->throwSkipTamperDataException($errorMessage);
        break;

      case self::BEHAVIOR_RAISE_EXCEPTION:
        $this->throwTamperException($errorMessage);
        break;
    }
    return $value;
  }

  /**
   * Skip current time to process.
   *
   * Note: Currently it causes skipping the whole process
   * because the skipping of item is not implemented yet.
   *
   * @param string $errorMessage
   *   The error message.
   *
   * @throws \Drupal\tamper\Exception\SkipTamperItemException
   */
  protected function throwSkipTamperDataException($errorMessage) {
    // @todo to use SkipTamperDataException when it will be working
    throw new SkipTamperItemException();
  }

  /**
   * Break the whole importing process.
   *
   * @param string $errorMessage
   *   The error message.
   *
   * @throws \Drupal\tamper\Exception\TamperException
   */
  protected function throwTamperException($errorMessage) {
    throw new TamperException($errorMessage);
  }

}
