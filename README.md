This module adds plugin for [Tamper](https://www.drupal.org/project/tamper).
The provided plugin uses a PHP function to process data. There are settings to describe arguments with tokens and settings for behavior if the function does not exist and if it throws exception.

##Usage

(For [Feeds Tamper](https://www.drupal.org/project/feeds_tamper))

1. Go to Tamper Tab
2. Click *Add plugin* and select *UserFunc* from *Custom* group.
3. Type function name
4. Describe arguments with tokens
5. Select fallback behavior if the function does not exist.
6. Select fallback behavior if the function throws exception.
7. Set Default value with tokens.

Supported fallback behaviors:
 - Set Default Value
 - Skip (make an item to not be processed)
 - Raise an exception (break process)
